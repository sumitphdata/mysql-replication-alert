#!/bin/bash

### VARIABLES ### \
EMAIL="example@mail.com"
SlaveHost="Enter the MySql Slave host name"
MYSQL_CHECK=$(mysql -u root -h $SlaveHost -p'{{ mysql_root_password }}' -e "SHOW VARIABLES LIKE '%version%';" || echo 1)
SSTATUS=$(mysql -u root -h $SlaveHost -p'{{ mysql_root_password }}' -e "SHOW SLAVE STATUS\G" | egrep "Master_Host|Master_User|Master_Log_File|Slave_IO_Running|Slave_SQL_Running|Last_Errno|Last_Error|Seconds_Behind_Master" | awk '{print $1,$2}')
LAST_ERRNO=$(mysql -u root -h $SlaveHost -p'{{ mysql_root_password }}' -e "SHOW SLAVE STATUS\G" | grep "Last_Errno" | awk '{ print $2 }')
SECONDS_BEHIND_MASTER=$(mysql -u root -h $SlaveHost -p'{{ mysql_root_password }}' -e "SHOW SLAVE STATUS\G"| grep "Seconds_Behind_Master" | awk '{ print $2 }')
IO_IS_RUNNING=$(mysql -u root -h $SlaveHost -p'{{ mysql_root_password }}' -e "SHOW SLAVE STATUS\G" | grep "Slave_IO_Running" | awk '{ print $2 }')
SQL_IS_RUNNING=$(mysql -u root -h $SlaveHost -p'{{ mysql_root_password }}' -e "SHOW SLAVE STATUS\G" | grep "Slave_SQL_Running" | awk '{ print $2 }' | awk 'FNR == 1')
ERRORS=()

### Run Some Checks ###

## Check if I can connect to Mysql ##

if [ "$MYSQL_CHECK" == 1 ]
then
    ERRORS=("${ERRORS[@]}" "ERROR: Can't connect to MySQL")
fi

## Check For Last Error ##
if [ "$LAST_ERRNO" != 0 ]
then
    ERRORS=("${ERRORS[@]}" "ERROR: Error when processing relay log")
fi

## Check if IO thread is running ##
if [ "$IO_IS_RUNNING" != "Yes" ]
then
    ERRORS=("${ERRORS[@]}" "ERROR: I/O thread for reading the master's binary log is not running")
fi

## Check for SQL thread ##
if [ "$SQL_IS_RUNNING" != "Yes" ]
then
    ERRORS=("${ERRORS[@]}" "ERROR: SQL thread for executing events in the relay log is not running")
fi

## Check how slow the slave is ##

if [[ "$SECONDS_BEHIND_MASTER" -gt 1800 ]]
then
ERRORS=("${ERRORS[@]}" "ERROR: The Slave is more than 30 minutes behind the master")
fi

## Check For Last Error ##
if [ "${#ERRORS[@]}" -gt 0 ]
then
    MESSAGE="Replication on the slave MySQL server $host_name has stopped working:\n\n
    $(for i in $(seq 0 ${#ERRORS[@]}) ; do echo "${ERRORS[$i]}\n" ; done)
    "
    echo -e $MESSAGE"\nMySql Slave Status: Replication has stopped working\n$SSTATUS" | mail -s "CLIENT-ENV-Mysql Replication Alert on Hostname: $host_name" ${EMAIL}
fi
